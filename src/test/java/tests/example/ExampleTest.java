package tests.example;

import org.testng.annotations.Test;

import core.plugins.LinkChecker;
import core.settings.Groups;
import pageobjects.Homepage;
import settings.TestSuiteRemote;

/**
 * Example Test Suite 
 * 
 * @author Philip Taddey
 */
public class ExampleTest extends TestSuiteRemote {
    
    @Test(description = "I want to see the homepage", groups = { Groups.DEV })
    public void exampleTest() {
        i.openHomepage(maven.getProperty("website"));
        i.see(Homepage.logo);
        i.see(Homepage.search);
        i.assertEquals(i.getAttributeValueOf(Homepage.searchBtn, "aria-label"), "Google-Suche");
        i.enterText(Homepage.search, "Testautomation");
    }
    
    @Test(description = "I want to check that all links on the homepage are valid", groups = { Groups.DEV })
    public void linkChecker() {
        i.openHomepage(maven.getProperty("website"));
        i.assertTrue(i.assertLinkIsValid(maven.getProperty("website")));
        LinkChecker checkLinks = new LinkChecker(driver.get());
        i.assertTrue(checkLinks.checkPage(maven.getProperty("website")));
    }
}