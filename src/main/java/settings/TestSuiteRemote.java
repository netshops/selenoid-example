package settings;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebDriver;
import org.testng.IHookCallBack;
import org.testng.IHookable;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import core.exceptions.DriverFactoryException;
import core.exceptions.TestRailAPIException;
import core.helper.ConfigHelper;
import core.models.Environment;
import core.models.Jira;
import core.plugins.JiraReport;
import core.plugins.TestRailClient;
import core.runner.Runner;
import core.settings.MavenDefaults;
import drivers.pool.RemoteDriver;

/**
 * Testsuite provides all necessary before and after methods for the tests
 * 
 * @author Philip Taddey
 * @since 1.8
 * @date 31.07.2015
 */
public class TestSuiteRemote implements IHookable {
    /* Logging */
    public Logger log = LogManager.getLogger(TestSuiteRemote.class);
    /* WebDriver */
    public ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
    /* Session ID  */
    public ThreadLocal<String> sessionId = new ThreadLocal<String>();
    /* Maven Defaults */
    public MavenDefaults maven = MavenDefaults.init();
    /* TestRail Test Case ID  */
    public static String testcase = null;
    /* JSON Object for TestRail Client*/
    JSONObject jsonRequest;
    /* Runner for test execution */
    public Runner i;
     /* TestRail API Client */
    public TestRailClient client = new TestRailClient(ProjectSettings.TESTRAIL_URL);
    /* JIRA Client */
    public JiraReport jiraReport = new JiraReport(ProjectSettings.JIRA_USER, ProjectSettings.JIRA_PASSWORD, ProjectSettings.JIRA_URL);
 
    /**
     * Get Environment from Maven
     * 
     * @param method
     * @return env Environment
     */
    private Environment getEnv(String method) {
        Environment env = ConfigHelper.setupEnvironment(maven.getAll(), this.getClass().getSimpleName(), method);
        return env;
    }
    
    @BeforeSuite(alwaysRun = true)
    public void beforeTestSuite(final ITestContext testContext) throws MalformedURLException, IOException, TestRailAPIException {
        log.trace("=======================================================");
        log.trace(" SELENOID TESTAUTOMATION FRAMEWORK");
        log.trace(" PROJECT: " + ProjectSettings.PROJECT_NAME);
        log.trace(" URL: "+ maven.getProperty("website"));
        log.trace("=======================================================");
    }
    
    @BeforeMethod(alwaysRun = true)
    public void before(ITestContext context) throws DriverFactoryException, IOException {
        Environment env = getEnv(context.getName());
        driver.set(RemoteDriver.getDriver(env));
        i = new Runner(driver.get());
    }
    
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResult) throws IOException, TestRailAPIException {
        Environment env = getEnv(testResult.getName());
        if (env.isTestrail() == true) {
            client.setUser(ProjectSettings.TESTRAIL_USER);
            client.setPassword(ProjectSettings.TESTRAIL_PASSWORD);
            Map<String, Object> data = new HashMap<>();
            if (testResult.getStatus() == ITestResult.SUCCESS) {
                data.put("status_id", new Integer(1));
                data.put("comment", "**Test successful!** \n\n" + "**Test Steps:** \n" + Reporter.getOutput().toString().replace(",", ""));
            } else if (testResult.getStatus() == ITestResult.FAILURE) {
                data.put("status_id", new Integer(5));
                data.put("comment", "**Test failed!** \n\n" + "**Test Steps:** \n" + Reporter.getOutput().toString().replace(",", "") + "\n\n **URL:** \n" + i.getCurrentUrl());       
            } else {
                data.put("status_id", new Integer(6));
                data.put("comment", "There was an error. Test not executed."); 
            }
            jsonRequest = (JSONObject) client.sendPost("add_result_for_case/" + maven.getProperty("testrail_id") + "/" + testcase, data);
        }
        Reporter.clear();
        driver.get().quit();
    }

    @Override
    public void run(IHookCallBack callBack, ITestResult testResult) {
        log.trace("=======================================================");
        log.trace("Test: " + testResult.getMethod().getDescription());
        log.trace("=======================================================");
        callBack.runTestMethod(testResult);
        if (testResult.getThrowable() != null) {
            try {
                File screenshot = i.takeScreenshot(testResult.getMethod().getMethodName());
                Environment env = getEnv(testResult.getName());
                if(env.isJira() == true) {
                    Jira ticket = new Jira(ProjectSettings.JIRA_PROJECT, ProjectSettings.PROJECT_NAME, i.getCurrentUrl(), 
                            maven.getProperty("environment"), env, testResult.getName(), ProjectSettings.JIRA_ASSIGNEE, 
                            Reporter.getOutput().toString(), testResult.getThrowable().getCause().toString(),
                            testResult.getTestContext().getStartDate().toString(), screenshot);
                    JiraReport.createSummary(ticket);
                    JiraReport.reportIssue(ticket);    
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}