package settings;

import core.models.Customer;

/**
 * Project Settings
 * 
 * @author Philip Taddey
 */
public class ProjectSettings {
    /** Project Info */
    public static final String PROJECT_NAME = "DEMO";
    /** API */
    public static final String SHOP_API = "";
    public static final String SHOP_API_USER = "";
    public static final String SHOP_API_PASSWORD = "";
    /** Voucher */
    public static final String VOUCHER_CODE = "";
    public static final String VOUCHER_NAME = "";
    public static final String VOUCHER_VALUE = "";
    /** JIRA Integration */
    public static final String JIRA_URL = "";
    public static final String JIRA_USER = "";
    public static final String JIRA_PASSWORD = "";
    public static final String JIRA_REPORTER = "";
    public static final String JIRA_ASSIGNEE = "";
    public static final String JIRA_PROJECT = "";
    /** TestRail Integration */
    public static final String TESTRAIL_URL = "";
    public static final String TESTRAIL_USER = "";
    public static final String TESTRAIL_PASSWORD = "";
    /** Testcustomer */
    public static Customer customer = new Customer(null, "testing@example.de", "12345678", "Ms", "Test", "Test", 
            "1", "1", "1970", null, "Street", "1", "12345", "City", "Ms Test Test\n Street 1\n12345 City\nDeutschland");
}