package pageobjects;

import org.openqa.selenium.By;

/**
 * Page Objects for Homepage
 */
public class Homepage {
    public static By logo = By.id("hplogo");
    public static By search = By.id("lst-ib");
    public static By searchBtn = By.name("btnK");
}

